from django.shortcuts import render
from django.http import HttpResponse

def home(request):
	return render (request,'story3.html')

def about(request):
	context = {"about_page": "active"}
	return render (request, 'story3.1.html',context)

def contact(request):
	context = {"contact_page" : "active"}
	return render (request, 'story3.3.html', context)


# Create your views here.
